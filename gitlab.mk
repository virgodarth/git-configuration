REPO_PROVIDER_URL=https://gitlab.com
TMP_BRANCH=tmp-branch-can-remove

$(repo_fork)%:
	$(eval repo_name=$(shell echo $* | cut -d',' -f2))
	@echo 'Forking $(repo_name) repository...'
	@curl --request POST --header "PRIVATE-TOKEN: $(APP_TOKEN)" "$(REPO_PROVIDER_URL)/api/v4/projects/$(shell echo $* | cut -d',' -f1)/fork?path=$(repo_name)&name=$(repo_name)"
	@echo
	@echo 'Forked $(repo_name) completed!'
	@echo

$(repo_clone)%:
	$(eval repo_name=$(shell echo $* | cut -d',' -f2))
	@echo 'Clone $(repo_name) repository into $(REPO_ROOT_DIR)'
	@cd $(REPO_ROOT_DIR) ; [ -d "${repo_name}" ] || git clone $(REPO_PROVIDER_URL)/$(ORIGIN_REPO)/$(repo_name).git
	@echo 'Cloned $(repo_name) repository'
	@cd $(REPO_ROOT_DIR)/$(repo_name); git config user.name $(NAME); git config user.email $(EMAIL)
	@echo 'Updated user.name ($(NAME)) and user.email $(EMAIL)'
	@echo 'Completed clone $(repo_name) repository.'
	@echo

$(repo_upstream)%:
	$(eval repo_name=$(shell echo $* | cut -d',' -f2))
	@echo 'Seting upstream for $(repo_name) repository'
	@cd $(REPO_ROOT_DIR)/$(repo_name); git remote -v | grep upstream -q || git remote add upstream $(REPO_PROVIDER_URL)/$(UPSTREAM_REPO)/$(repo_name).git
	@cd $(REPO_ROOT_DIR)/$(repo_name); git remote set-url upstream $(REPO_PROVIDER_URL)/$(ORIGIN_REPO)/$(repo_name).git
	@cd $(REPO_ROOT_DIR)/$(repo_name); git remote set-url upstream $(REPO_PROVIDER_URL)/$(UPSTREAM_REPO)/$(repo_name).git
	@cd $(REPO_ROOT_DIR)/$(repo_name); git remote -v
	@cd $(REPO_ROOT_DIR)/$(repo_name); git fetch --all
	@cd $(REPO_ROOT_DIR)/$(repo_name); git checkout -b $(TMP_BRANCH) 2>/dev/null || git checkout $(TMP_BRANCH)
	@cd $(REPO_ROOT_DIR)/$(repo_name); git branch -d main && git checkout --track upstream/main 2>/dev/null
	@cd $(REPO_ROOT_DIR)/$(repo_name); (git branch |grep develop -q && git branch -d develop) || (git checkout --track upstream/develop && git branch -d $(TMP_BRANCH))
	@echo 'Completed set upstream for $(repo_name) repository'
	@echo

$(repo_fetch)%:
	$(eval repo_name=$(shell echo $* | cut -d',' -f2))
	@echo 'Fetching data for $(repo_name) repository'
	@cd $(REPO_ROOT_DIR)/$(repo_name); git fetch --all
	@echo 'Completed fetch data for $(repo_name) repository'
	@echo

$(repo_pull_master)%:
	$(eval repo_name=$(shell echo $* | cut -d',' -f2))
	$(eval current_branch=$(shell cd $(REPO_ROOT_DIR)/$(repo_name);git rev-parse --abbrev-ref HEAD))
	@echo 'Pulling out main branch for $(repo_name) repository'
	@cd $(REPO_ROOT_DIR)/$(repo_name); git checkout main 2>/dev/null && (git pull; git checkout $(current_branch) 2>/dev/null)
	@echo 'Completed pull main branch for $(repo_name) repository'
	@echo

$(repo_pull_develop)%:
	$(eval repo_name=$(shell echo $* | cut -d',' -f2))
	$(eval current_branch=$(shell cd $(REPO_ROOT_DIR)/$(repo_name);git rev-parse --abbrev-ref HEAD))
	@echo 'Pulling out develop branch for $(repo_name) repository'
	@cd $(REPO_ROOT_DIR)/$(repo_name); git checkout develop 2>/dev/null && (git pull; git checkout $(current_branch))
	@echo 'Completed pull develop branch for $(repo_name) repository'
	@echo

$(repo_checkout_master)%:
	@echo $*
	$(eval repo_name=$(shell echo $* | cut -d',' -f2))
	@echo 'Checking out main branch for $(repo_name) repository'
	@cd $(REPO_ROOT_DIR)/$(repo_name); git checkout main 2>/dev/null && git pull 2>/dev/null
	@echo 'Completed checked out main branch for $(repo_name) repository'
	@echo

$(repo_checkout_develop)%:
	$(eval repo_name=$(shell echo $* | cut -d',' -f2))
	@echo $(repo_name)
	@echo 'Checking out develop branch for $(repo_name) repository'
	@cd $(REPO_ROOT_DIR)/$(repo_name); git checkout develop 2>/dev/null && git pull 2>/dev/null
	@echo 'Completed checked out develop branch for $(repo_name) repository'
	@echo

$(repo_reset)%:
	$(eval repo_name=$(shell echo $* | cut -d',' -f2))
	@echo 'Reset $(repo_name) repository...'
	@cd $(REPO_ROOT_DIR)/$(repo_name); git checkout .
	@echo 'Reset $(repo_name) completed!'

$(repo_status)%:
	$(eval repo_name=$(shell echo $* | cut -d',' -f2))
	@echo 'Git status $(repo_name) repository...'
	@cd $(REPO_ROOT_DIR)/$(repo_name); git status
	@echo 'Completed!'
	@echo