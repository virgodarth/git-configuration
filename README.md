# Git Configuration

## Overview
This repository is a collection of tools and scripts that're uses to make it simple to work with git. The purpose of this repository is to share portions of our toolchain with the community. This repository is not the best way to get started git, but I hope this is useful when you interact with more repo in the same time.

The tool is support gitlab.com and bitbucket.org on Linux, Mac and Windows. In the future, the script will support github.com and other git repositories.
- envs: local enviroments, please don't share with anyone
- repository: repositories you want to manage

## Get Starting
1. Clone local.example.env to local.env
```
$ mv local.example.env local.env
```

2. Add enviroments
- REPO_PROVIDER: repository provider. Choose: gitlab/ bitbucket
- APP_TOKEN: app's access key on [gitlab.org](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) or [bitbucket](https://support.atlassian.com/bitbucket-cloud/docs/app-passwords/)
- USERNAME: username which you registered with git version control
- UPSTREAM_REPO: upstream repository name
- ORIGIN_REPO: origin repository name
- NAME: your name
- EMAIL: your email
- REPO_ROOT_DIR: place your workspace. Default: the same git-configuration repository

3. Clone bitbucket/gitlab.example.txt to bitbucket/gitlab.txt
```
$ mv bitbucket.example.txt bitbucket.txt
# or
$ mv gitlab.example.txt gitlab.txt
```

4. Add repositories on gitlab or bitbucket. Each repository per line
- Gitlab: {{project_id}},{{repository_name}}
- Bitbucket: {{repository_name}}

5. Run ```make help``` to detail of command
```
Help:
    $repo_name: any repository name

    fork        forks all repositories in repositories/$repo_provider.txt
    clone       clone all repositories in repositories/$repo_provider.txt
    upstream    set up upstream for all repositories in repositories/$repo_provider.txt
    fetch       fetch all repositories in repositories/$repo_provider.txt
    pull.master/ pull.develop       pull master/ develop branch for all repositories in repositories/$repo_provider.txt
    checkout.master/ checkout.develop       checkout master/ develop branch for all repositories in repositories/$repo_provider.txt

    fork.$repo_name   fork specific $repo_name
    clone.$repo_name     clone specific $repo_name
    fetch.$repo_name    fetch specific $repo_name
    upstream.$repo_name    set up upstream for specific $repo_name
    pull.master.$repo_name    pull master branch for specific $repo_name
    pull.develop.$repo_name    pull develop branch for specific $repo_name
    checkout.master.$repo_name    checkout master branch for specific $repo_name
    checkout.develop.$repo_name    checkout develop branch for specific $repo_name
```

# Example
```
# Fork repositories from upstream repository to your own repository
$ make fork

# Clone all repositories on your own repository
$ make clone

# Set up upstream for all repositories
$ make upstream

# Fetch all repositories
$ make fetch

# Pull latest code on develop branch for all repositories
$ make pull.develop

$ Checkout develop branch for all repositories
$ make checkout.develop
```