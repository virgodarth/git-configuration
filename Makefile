include envs/local.env

repos:=$(shell cat "./repositories/$(REPO_PROVIDER).txt")

repo_reset=repo.reset.
repo_status=repo.status.
repo_fork=repo.fork.
repo_clone=repo.clone.
repo_fetch=repo.fetch.
repo_upstream=repo.upstream.
repo_pull_master=repo.pull.master.
repo_pull_develop=repo.pull.develop.
repo_checkout_master=repo.checkout.master.
repo_checkout_develop=repo.checkout.develop.

help:
	@echo '    Help:'
	@echo '        $$repo_name: any repository name'
	@echo ''
	@echo '        reset       resets all repositories in repositories/$$repo_provider.txt'
	@echo '        fork        forks all repositories in repositories/$$repo_provider.txt'
	@echo '        clone       clone all repositories in repositories/$$repo_provider.txt'
	@echo '        upstream    set up upstream for all repositories in repositories/$$repo_provider.txt'
	@echo '        fetch       fetch all repositories in repositories/$$repo_provider.txt'
	@echo '        pull.master/ pull.develop       pull master/ develop branch for all repositories in repositories/$$repo_provider.txt'
	@echo '        checkout.master/ checkout.develop       checkout master/ develop branch for all repositories in repositories/$$repo_provider.txt'
	@echo ''
	@echo '        $(repo_status)$$repo_name   get status of specific $$repo_name'
	@echo '        $(repo_reset)$$repo_name   reset specific $$repo_name'
	@echo '        $(repo_fork)$$repo_name   fork specific $$repo_name'
	@echo '        $(repo_clone)$$repo_name     clone specific $$repo_name'
	@echo '        $(repo_fetch)$$repo_name    fetch specific $$repo_name'
	@echo '        $(repo_upstream)$$repo_name    set up upstream for specific $$repo_name'
	@echo '        $(repo_pull_master)$$repo_name    pull master branch for specific $$repo_name'
	@echo '        $(repo_pull_develop)$$repo_name    pull develop branch for specific $$repo_name'
	@echo '        $(repo_checkout_master)$$repo_name    checkout master branch for specific $$repo_name'
	@echo '        $(repo_checkout_develop)$$repo_name    checkout develop branch for specific $$repo_name'
	@echo ''

reset: resets
status: statuses
fork: forks
clone: clones
upstream: upstreams
fetch: fetches
pull.master: pull.masters
pull.develop: pull.develops
checkout.master: checkout.masters
checkout.develop: checkout.develops

resets: $(foreach repo,$(repos),$(repo_reset)$(repo))
statuses: $(foreach repo,$(repos),$(repo_status)$(repo))
forks: $(foreach repo,$(repos),$(repo_fork)$(repo))
clones: $(foreach repo,$(repos),$(repo_clone)$(repo))
upstreams: $(foreach repo,$(repos),$(repo_upstream)$(repo))
fetches: $(foreach repo,$(repos),$(repo_fetch)$(repo))
pull.masters: $(foreach repo,$(repos),$(repo_pull_master)$(repo))
pull.develops: $(foreach repo,$(repos),$(repo_pull_develop)$(repo))
checkout.masters: $(foreach repo,$(repos),$(repo_checkout_master)$(repo))
checkout.develops: $(foreach repo,$(repos),$(repo_checkout_develop)$(repo))
	
include $(REPO_PROVIDER).mk