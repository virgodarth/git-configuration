REPO_PROVIDER_URL=https://bitbucket.org
TMP_BRANCH=tmp-branch-can-remove

$(repo_fork)%:
	$(eval repo_name=$*)
	@echo 'Forking $(repo_name) repository...'
	@curl --request POST -u $(USERNAME):$(APP_TOKEN) https://api.bitbucket.org/2.0/repositories/$(UPSTREAM_REPO)/$(repo_name)/forks
	@echo
	@echo 'Forked $(repo_name) completed!'
	@echo

$(repo_clone)%:
	$(eval repo_name=$*)
	@echo 'Clone $(repo_name) repository into $(REPO_ROOT_DIR)'
	@cd $(REPO_ROOT_DIR) ; [ -d "${repo_name}" ] || git clone $(REPO_PROVIDER_URL)/$(ORIGIN_REPO)/$(repo_name).git
	@echo 'Cloned $(repo_name) repository'
	@cd $(REPO_ROOT_DIR)/$(repo_name); git config user.name $(USERNAME); git config user.email $(EMAIL)
	@echo 'Updated user.name ($(USERNAME)) and user.email $(EMAIL)'
	@echo 'Completed clone $(repo_name) repository.'
	@echo

$(repo_upstream)%:
	$(eval repo_name=$*)
	@echo 'Seting upstream for $(repo_name) repository'
	@cd $(REPO_ROOT_DIR)/$(repo_name); git remote -v | grep upstream -q || git remote add upstream $(REPO_PROVIDER_URL)/$(UPSTREAM_REPO)/$(repo_name).git
	@cd $(REPO_ROOT_DIR)/$(repo_name); git remote set-url upstream $(REPO_PROVIDER_URL)/$(ORIGIN_REPO)/$(repo_name).git
	@cd $(REPO_ROOT_DIR)/$(repo_name); git remote set-url upstream $(REPO_PROVIDER_URL)/$(UPSTREAM_REPO)/$(repo_name).git
	@cd $(REPO_ROOT_DIR)/$(repo_name); git remote -v
	@cd $(REPO_ROOT_DIR)/$(repo_name); git fetch --all
	@cd $(REPO_ROOT_DIR)/$(repo_name); git checkout -b $(TMP_BRANCH) 2>/dev/null || git checkout $(TMP_BRANCH)
	@cd $(REPO_ROOT_DIR)/$(repo_name); git branch -d master && git checkout --track upstream/master 2>/dev/null
	@cd $(REPO_ROOT_DIR)/$(repo_name); (git branch |grep develop -q && git branch -d develop) || (git checkout --track upstream/develop && git branch -d $(TMP_BRANCH))
	@echo 'Completed set upstream for $(repo_name) repository'
	@echo

$(repo_fetch)%:
	$(eval repo_name=$*)
	@echo 'Fetching data for $(repo_name) repository'
	@cd $(REPO_ROOT_DIR)/$(repo_name); git fetch --all
	@echo 'Completed fetch data for $(repo_name) repository'
	@echo

$(repo_pull_master)%:
	$(eval repo_name=$*)
	@echo $(REPO_ROOT_DIR)/$(repo_name)
	$(eval current_branch=$(shell cd $(REPO_ROOT_DIR)/$(repo_name);git rev-parse --abbrev-ref HEAD))
	@echo 'Pulling master branch for $(repo_name) repository'
	@cd $(REPO_ROOT_DIR)/$(repo_name); git checkout master 2>/dev/null && (git pull; git checkout $(current_branch) 2>/dev/null)
	@echo 'Completed pull master branch for $(repo_name) repository'
	@echo

$(repo_pull_develop)%:
	$(eval repo_name=$*)
	$(eval current_branch=$(shell cd $(REPO_ROOT_DIR)/$(repo_name);git rev-parse --abbrev-ref HEAD))
	@echo 'Pulling develop branch for $(repo_name) repository'
	@cd $(REPO_ROOT_DIR)/$(repo_name); git checkout develop 2>/dev/null && (git pull; git checkout $(current_branch))
	@echo 'Completed pull develop branch for $(repo_name) repository'
	@echo

$(repo_checkout_master)%:
	$(eval repo_name=$*)
	@echo 'Checking out master branch for $(repo_name) repository'
	@cd $(REPO_ROOT_DIR)/$(repo_name); git checkout master 2>/dev/null && git pull 2>/dev/null
	@echo 'Completed checked out master branch for $(repo_name) repository'
	@echo

$(repo_checkout_develop)%:
	$(eval repo_name=$*)
	@echo 'Checking out develop branch for $(repo_name) repository'
	@cd $(REPO_ROOT_DIR)/$(repo_name); git checkout develop 2>/dev/null && git pull 2>/dev/null
	@echo 'Completed checked out develop branch for $(repo_name) repository'
	@echo

$(repo_reset)%:
	$(eval repo_name=$*)
	@echo 'Reset $(repo_name) repository...'
	@cd $(REPO_ROOT_DIR)/$(repo_name); git checkout .
	@echo 'Reset $(repo_name) completed!'

$(repo_status)%:
	$(eval repo_name=$*)
	@echo 'Git status $(repo_name) repository...'
	@cd $(REPO_ROOT_DIR)/$(repo_name); git status
	@echo 'Completed!'
	@echo